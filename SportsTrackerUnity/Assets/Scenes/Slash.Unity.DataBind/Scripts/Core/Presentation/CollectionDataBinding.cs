namespace Slash.Unity.DataBind.Core.Presentation
{
    using System;
    using System.Collections.Generic;

    using Slash.Unity.DataBind.Core.Data;
    
    /// <summary>
    ///   Utility class for a data binding of a collection.
    ///   Often we have to react on item added/removed/inserted events and those actions have to be
    ///   moved over to a new bound collection when it changes.
    ///   This class takes over the task to observe the collection and registers for its events.
    /// </summary>
    /// <typeparam name="TItem">Type of items in the collection.</typeparam>
    public class CollectionDataBinding<TItem>
    {
        private readonly DataBinding collectionBinding;

        /// <summary>
        ///   Action to execute when the items were cleared.
        /// </summary>
        public Action ClearedItems;

        /// <summary>
        ///   Action to execute when the collection changed.
        /// </summary>
        public Action<Collection> CollectionChanged;

        /// <summary>
        ///   Action to execute when an item was added to the collection.
        /// </summary>
        public Action<TItem> ItemAdded;

        /// <summary>
        ///   Action to execute when an item was inserted to the collection.
        /// </summary>
        public Action<int, TItem> ItemInserted;

        /// <summary>
        ///   Action to execute when an item was removed from the collection.
        /// </summary>
        public Action<TItem> ItemRemoved;

        private Collection collection;

        /// <summary>
        ///   Constructor.
        /// </summary>
        /// <param name="collectionBinding">Collection binding to observe.</param>
        public CollectionDataBinding(DataBinding collectionBinding)
        {
            this.collectionBinding = collectionBinding;
        }

        /// <summary>
        ///   Current collection that the binding provides.
        /// </summary>
        public Collection Collection
        {
            get
            {
                return this.collection;
            }
            private set
            {
                if (value == this.collection)
                {
                    return;
                }
                if (this.collection != null)
                {
                    this.collection.ItemAdded -= this.OnItemAdded;
                    this.collection.ItemRemoved -= this.OnItemRemoved;
                    this.collection.ItemInserted -= this.OnItemInserted;
                    this.collection.ClearedItems -= this.OnClearedItems;
                }

                this.collection = value;

                if (this.collection != null)
                {
                    this.collection.ItemAdded += this.OnItemAdded;
                    this.collection.ItemRemoved += this.OnItemRemoved;
                    this.collection.ItemInserted += this.OnItemInserted;
                    this.collection.ClearedItems += this.OnClearedItems;
                }

                if (this.CollectionChanged != null)
                {
                    this.CollectionChanged(this.collection);
                }
            }
        }

        /// <summary>
        ///   Disables the observer.
        /// </summary>
        public void Disable()
        {
            this.collectionBinding.ValueChanged -= this.OnCollectionChanged;
            this.Collection = null;
        }

        /// <summary>
        ///   Enables the observer.
        /// </summary>
        public void Enable()
        {
            this.collectionBinding.ValueChanged += this.OnCollectionChanged;
            this.Collection = this.collectionBinding.GetValue<Collection>();
        }

        private void OnClearedItems(IEnumerable<object> items)
        {
            if (this.ClearedItems != null)
            {
                this.ClearedItems();
            }
        }

        private void OnCollectionChanged(object newValue)
        {
            this.Collection = (Collection)newValue;
        }

        private void OnItemAdded(object item)
        {
            if (this.ItemAdded != null)
            {
                this.ItemAdded((TItem)item);
            }
        }

        private void OnItemInserted(object item, int index)
        {
            if (this.ItemInserted != null)
            {
                this.ItemInserted(index, (TItem)item);
            }
        }

        private void OnItemRemoved(object item)
        {
            if (this.ItemRemoved != null)
            {
                this.ItemRemoved((TItem)item);
            }
        }
    }
}