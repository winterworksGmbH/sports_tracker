namespace Slash.Unity.DataBind.Foundation.Providers.Getters
{
    using Slash.Unity.DataBind.Core.Data;
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Core.Utils;

    using UnityEngine;

    /// <summary>
    ///   Base class to get a data value from a bound context.
    /// </summary>
    public class ContextDataProvider : DataProvider
    {
        /// <summary>
        ///   Data binding for context to get data from.
        /// </summary>
        [Tooltip("Data binding for context to get data from")]
        public DataBinding Context;

        /// <summary>
        ///   Type of context the binding provides.
        ///   Used to show the path drop down.
        /// </summary>
        [ContextType]
        [Tooltip("Type of context the binding provides.")]
        public string ContextType;

        /// <summary>
        ///   Path to get data from context from.
        /// </summary>
        [Tooltip("Path to get data from context from")]
        [ContextPath(Filter = ~ContextMemberFilter.Methods | ~ContextMemberFilter.Contexts)]
        public string Path;

        /// <summary>
        ///   Current data value.
        /// </summary>
        private object data;

        /// <inheritdoc />
        public override object Value
        {
            get
            {
                return this.data;
            }
        }

        /// <inheritdoc />
        public override void Deinit()
        {
            base.Deinit();
            this.RemoveBinding(this.Context);
        }

        /// <inheritdoc />
        public override void Init()
        {
            base.Init();
            this.AddBinding(this.Context);
        }

        /// <summary>
        ///   Get the data value from the specified context.
        ///   Context is guaranteed to be not null.
        /// </summary>
        /// <param name="context">Context to get data value from.</param>
        /// <returns>Data value from specified context.</returns>
        protected object GetDataValue(Context context)
        {
            return context.GetValue(this.Path);
        }

        /// <inheritdoc />
        protected override void UpdateValue()
        {
            var context = this.Context.GetValue<Context>();
            var newData = context != null ? this.GetDataValue(context) : null;
            if (!Equals(newData, this.data))
            {
                this.data = newData;
                this.OnValueChanged();
            }
        }
    }
}