﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContextDataUpdater.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.Foundation.Setters
{
    using Slash.Unity.DataBind.Core.Presentation;
    using Slash.Unity.DataBind.Core.Utils;

    using UnityEngine;

    /// <summary>
    ///   Updates a data property of a context with the value of a data binding.
    /// </summary>
    [AddComponentMenu("Data Bind/Foundation/Setters/[DB] Context Data Updater")]
    public class ContextDataUpdater : DataBindingOperator
    {
        /// <summary>
        ///   Data to update context data from.
        /// </summary>
        public DataBinding Data;

        /// <summary>
        ///   Path to value to update in data context.
        /// </summary>
        [ContextPath(Filter = ~ContextMemberFilter.Methods | ~ContextMemberFilter.Contexts)]
        public string Path;

        private ContextNode node;

        /// <inheritdoc />
        public override void Deinit()
        {
            base.Deinit();
            this.RemoveBinding(this.Data);
        }

        /// <inheritdoc />
        public override void Disable()
        {
            base.Disable();

            this.Data.ValueChanged -= this.OnDataChanged;
        }

        /// <inheritdoc />
        public override void Enable()
        {
            base.Enable();

            this.Data.ValueChanged += this.OnDataChanged;
            if (this.Data.IsInitialized)
            {
                this.UpdateDataValue(this.Data.Value);
            }
        }

        /// <inheritdoc />
        public override void Init()
        {
            base.Init();
            this.node = new ContextNode(this.gameObject, this.Path);
            this.AddBinding(this.Data);
        }

        /// <inheritdoc />
        public override void OnContextChanged()
        {
            base.OnContextChanged();

            if (this.node == null)
            {
                return;
            }

            this.node.OnHierarchyChanged();

            // Update value.
            this.UpdateDataValue(this.Data.Value);
        }

        private void OnDataChanged(object newValue)
        {
            this.UpdateDataValue(newValue);
        }

        private void UpdateDataValue(object value)
        {
            this.node.SetValue(value);
        }
    }
}