﻿namespace Slash.Unity.DataBind.Foundation.Utils
{
    using System;
    using System.Collections.Generic;

    using Slash.Unity.DataBind.Core.Data;

    /// <summary>
    ///   Utility class to observe the items of a collection.
    ///   Can be used e.g. when the single items have an event that should be forwarded to a common context event.
    /// </summary>
    /// <typeparam name="TItem">Type of items in the collection.</typeparam>
    public class CollectionObserver<TItem>
    {
        private readonly List<TItem> registeredItems = new List<TItem>();

        private Collection<TItem> collection;

        /// <summary>
        ///   Action to call after an item enters the collection.
        /// </summary>
        public event Action<TItem> RegisterItem;

        /// <summary>
        ///   Action to call before an item leaves the collection.
        /// </summary>
        public event Action<TItem> UnregisterItem;

        /// <summary>
        ///   Deinitializes the observer.
        /// </summary>
        public void Deinit()
        {
            this.OnCleared();

            if (this.collection != null)
            {
                this.collection.ItemAdded -= this.OnItemAdded;
                this.collection.ItemRemoved -= this.OnItemRemoved;
                this.collection.Cleared -= this.OnCleared;
                this.collection = null;
            }
        }

        /// <summary>
        ///   Initializes the observer.
        /// </summary>
        /// <param name="collectionToObserve">Collection to observe.</param>
        public void Init(Collection<TItem> collectionToObserve)
        {
            this.collection = collectionToObserve;
            this.collection.ItemAdded += this.OnItemAdded;
            this.collection.ItemRemoved += this.OnItemRemoved;
            this.collection.Cleared += this.OnCleared;

            this.registeredItems.Clear();
        }

        private void OnCleared()
        {
            foreach (var item in this.registeredItems)
            {
                this.OnUnregisterItem(item);
            }
            this.registeredItems.Clear();
        }

        private void OnItemAdded(object item)
        {
            var castedItem = (TItem)item;
            this.OnRegisterItem(castedItem);
            this.registeredItems.Add(castedItem);
        }

        private void OnItemRemoved(object item)
        {
            var castedItem = (TItem)item;
            this.OnUnregisterItem(castedItem);
            this.registeredItems.Remove(castedItem);
        }

        private void OnRegisterItem(TItem item)
        {
            var handler = this.RegisterItem;
            if (handler != null)
            {
                handler(item);
            }
        }

        private void OnUnregisterItem(TItem item)
        {
            var handler = this.UnregisterItem;
            if (handler != null)
            {
                handler(item);
            }
        }
    }
}