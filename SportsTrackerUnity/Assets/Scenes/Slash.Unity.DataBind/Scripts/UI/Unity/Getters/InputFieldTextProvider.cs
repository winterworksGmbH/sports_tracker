﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputFieldTextProvider.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Slash.Unity.DataBind.UI.Unity.Getters
{
    using Slash.Unity.DataBind.Foundation.Providers.Getters;
    using UnityEngine;
    using UnityEngine.UI;

    /// <summary>
    ///     Provides the text of an input field.
    /// </summary>
    [AddComponentMenu("Data Bind/UnityUI/Getters/[DB] Input Field Text Provider (Unity)")]
    public class InputFieldTextProvider : ComponentDataProvider<InputField, string>
    {
        /// <inheritdoc />
        protected override void AddListener(InputField target)
        {
            target.onValueChanged.AddListener(this.OnTargetValueChanged);
        }

        /// <inheritdoc />
        protected override string GetValue(InputField target)
        {
            return target.text;
        }

        /// <inheritdoc />
        protected override void RemoveListener(InputField target)
        {
            target.onValueChanged.RemoveListener(this.OnTargetValueChanged);
        }

        private void OnTargetValueChanged(string newValue)
        {
            this.OnTargetValueChanged();
        }
    }
}