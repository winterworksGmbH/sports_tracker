﻿using UnityEngine;
using VexatronApp.Templates;

namespace VexatronApp.Consts
{
    public class ConstsColors : Singleton<ConstsColors>
    {
        public Color ColorBackgroundDark = new Color(6,6, 17);
        public Color ColorBackgroundMedium = new Color(14, 14, 25);
        public Color ColorBackgroundLight = new Color(21, 23, 35);
        public Color ColorHeartBeatHeartCircle = new Color(255, 37, 126);
    }
}