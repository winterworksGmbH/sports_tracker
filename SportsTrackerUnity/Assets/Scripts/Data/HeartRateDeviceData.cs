﻿// this class holds information about the heart beat
// it is built up from json data sent by the device

using System;

namespace VexatronApp.Data
{
    [Serializable]
    public class HeartRateDeviceData : DeviceDataBase
    {
        public HeartRateDeviceData() :this(90)
        {
        }

        public HeartRateDeviceData(uint _bpm)
        {
            Bpm = _bpm;
        }

        public uint Bpm = 0;
    }
}
