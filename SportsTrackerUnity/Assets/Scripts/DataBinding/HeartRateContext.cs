﻿using UnityEngine;
using Slash.Unity.DataBind.Core.Data;
using VexatronApp.Interfaces;
using System;
using VexatronApp.Events;
using VexatronApp.Data;

namespace VexatronApp.DataBinding
{
    public class HeartRateContext : ContextBase
    {
        private readonly Property<uint> bpmProperty = new Property<uint>();

        public HeartRateContext()
        {
            VexatronEvents.Instance.AddListener<VexatronEventDeviceDataUpdate<HeartRateDeviceData>>(OnUpdateBpm);
        }

        public uint Bpm
        {
            get { return bpmProperty.Value; }
            set { bpmProperty.Value = value; }
        }

        // test for changing Bpm value
        public void OnUpdateBpm(VexatronEventDeviceDataUpdate<HeartRateDeviceData> _event)
        {
            Bpm = _event.Data.Bpm;
        }
    }
}