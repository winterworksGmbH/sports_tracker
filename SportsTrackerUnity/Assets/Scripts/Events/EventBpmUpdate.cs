﻿// generic event class for device data events
namespace VexatronApp.Events
{
    public class VexatronEventDeviceDataUpdate<T> : VexatronEventBase
    {
        private readonly T m_data;

        public VexatronEventDeviceDataUpdate(T _data)
        {
            m_data = _data;
        }

        public T Data
        {
            get { return m_data; }
        }
    }
}
