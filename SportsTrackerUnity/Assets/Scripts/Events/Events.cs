﻿// simple messaging that does not need a MonoBehaviour to work (like all other Unity Event stuff)
using System;
using System.Collections.Generic;
using VexatronApp.Templates;

namespace VexatronApp.Events
{
    public class VexatronEvents : Singleton<VexatronEvents>
    {
        public delegate void EventDelegate<T>(T e) where T : VexatronEventBase;
        private delegate void EventDelegate(VexatronEventBase e);

        private Dictionary<Type, EventDelegate> delegates = new Dictionary<Type, EventDelegate>();
        private Dictionary<Delegate, EventDelegate> delegateLUT = new Dictionary<Delegate, EventDelegate>();

        public void AddListener<T>(EventDelegate<T> _delegate) where T : VexatronEventBase
        {
            // listener already registered? then quit
            if (delegateLUT.ContainsKey(_delegate))
                return;

            // Create a new non-generic delegate which calls our generic one.
            // This is the delegate we actually invoke.
            EventDelegate internalDelegate = (e) => _delegate((T)e);
            delegateLUT[_delegate] = internalDelegate;

            EventDelegate tempDel;
            if (delegates.TryGetValue(typeof(T), out tempDel))
            {
                delegates[typeof(T)] = tempDel += internalDelegate;
            }
            else
            {
                delegates[typeof(T)] = internalDelegate;
            }
        }

        public void RemoveListener<T>(EventDelegate<T> del) where T : VexatronEventBase
        {
            EventDelegate internalDelegate;
            if (delegateLUT.TryGetValue(del, out internalDelegate))
            {
                EventDelegate tempDel;
                if (delegates.TryGetValue(typeof(T), out tempDel))
                {
                    tempDel -= internalDelegate;
                    if (tempDel == null)
                    {
                        delegates.Remove(typeof(T));
                    }
                    else
                    {
                        delegates[typeof(T)] = tempDel;
                    }
                }

                delegateLUT.Remove(del);
            }
        }

        public void Invoke(VexatronEventBase e)
        {
            EventDelegate del;
            if (delegates.TryGetValue(e.GetType(), out del))
            {
                del.Invoke(e);
            }
        }
    }
}