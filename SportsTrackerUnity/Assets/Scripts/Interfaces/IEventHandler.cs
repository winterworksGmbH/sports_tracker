﻿using UnityEngine.EventSystems;

namespace VexatronApp.Interfaces
{
    public interface IHeartRateEventHandler : IEventSystemHandler
    {
        // functions that can be called via the messaging system
        void OnUpdateBpm();
    }
}
