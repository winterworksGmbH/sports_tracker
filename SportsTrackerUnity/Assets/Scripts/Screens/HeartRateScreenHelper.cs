﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections;
using VexatronApp.Data;
using VexatronApp.DataBinding;
using Slash.Unity.DataBind.Core.Presentation;
using VexatronApp.Consts;
using VexatronApp.Interfaces;
using System;
using UnityEngine.EventSystems;
using VexatronApp.Events;

namespace VexatronApp.Screens
{
    public class HeartRateScreenHelper : HelperScreenBase
    {
        private float timer = 0.0f;        

        public float HeartPunchSize;
        public float HeartPunchDuration;
        public float HeartPunchElasticity;
        public int HeartPunchVibrato;
        public float HeartPunchLag;
        public float HeartScaleMultiplier;
        public GameObject[] Circles;
        public GameObject Heart;

        // Use this for initialization
        void Start()
        {
            InitElements();
            InitContext<HeartRateContext>();
            InitFromData(new HeartRateDeviceData());
            
            GetComponent<ContextHolder>().SetContext(m_context, "Bpm");
        }        

        public void TriggerBpmChange()
        {
            VexatronEvents.Instance.Invoke(new VexatronEventDeviceDataUpdate<HeartRateDeviceData>(new HeartRateDeviceData((uint)UnityEngine.Random.Range(50.0f, 200.0f))));
        }


        void Update()
        {
            timer -= Time.deltaTime;

            // bmp timer is done?
            if (timer <= 0.0f)
            {
                // calc new bpm
                timer = 60.0f / ((HeartRateContext)m_context).Bpm;

                // trigger punch animation
                StartCoroutine(TriggerBeat());
            }
        }

        protected override void InitElements()
        {
            // background
            Camera.main.backgroundColor = Consts.ConstsColors.Instance.ColorBackgroundMedium;

            // find all circles
            GameObject[] objects = GameObject.FindGameObjectsWithTag(Tags.HEART_RATE_CIRCLE);
            Color newColor = ConstsColors.Instance.ColorHeartBeatHeartCircle;
            Image img = null;

            // change color to consts color
            foreach (GameObject o in objects)
            {
                // set color but keep alpha
                img = o.GetComponent<Image>();
                img.color = new Color(newColor.r, newColor.g, newColor.b, img.color.a);
            }
        }

        private void Punch(GameObject _gameObject, float _multiplier)
        {
            Image img = _gameObject.GetComponent<Image>();
            _gameObject.transform.DOComplete();
            _gameObject.transform.DOPunchScale(new Vector3(HeartPunchSize * _multiplier, HeartPunchSize * _multiplier, 0.0f), HeartPunchDuration, HeartPunchVibrato, HeartPunchElasticity);
        }

        IEnumerator TriggerBeat()
        {
            // punch heart
            Punch(Heart, HeartScaleMultiplier);
            yield return new WaitForSeconds(HeartPunchLag);

            // punch circles
            for (int i = 0; i < Circles.Length; i++)
            {
                Punch(Circles[i], 1.0f);
                yield return new WaitForSeconds(HeartPunchLag);
            }
        }
    }
}