﻿using VexatronApp.Data;
using VexatronApp.DataBinding;
using System;
using System.Reflection;
using UnityEngine;

namespace VexatronApp.Screens
{
    public abstract class HelperScreenBase : MonoBehaviour
    {
        // m_context is the ViewModel in our MVVM pattern
        protected ContextBase m_context;

        // init UI elements once, mainly image references and colors
        protected abstract void InitElements();

        // init corresponding context class
        // TODO: ask Rico how to do this properly!
        protected void InitContext<T>()
        {
            m_context = (ContextBase)Activator.CreateInstance(typeof(T));
        }

        // TODO: and here, too
        // generic init context object with data from device
        // it does not matter which data class is passed.
        // the fields of _data are read via reflection,
        // then a corresponding property is looked up in m_context
        // and set accordingly if found
        protected virtual void InitFromData(DeviceDataBase _data)
        {
            PropertyInfo p;

            // step through all fields of data
            foreach (FieldInfo fi in _data.GetType().GetFields())
            {
                // check if a property exists
                p = m_context.GetType().GetProperty(fi.Name);
                if (null != p)
                {
                    // set property value to field value from data
                    p.SetValue(m_context, fi.GetValue(_data), null);
                }
            }
        }
    }
}
